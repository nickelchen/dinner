# encoding: utf-8

Setting.block_order = false
Setting.now_is_blocking_ordering_you_are_late = "你来晚了一步，现在已经不能点餐了。点餐时间是上午 9:30 ～ 10:30, 下午 2:30 ~ 4:30，下次要早点哦！"